extends Node2D

master var slave_position = Vector2()
master var slave_rotation = 0

# Called when the node enters the scene tree for the first time.
func _ready():
    pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#    pass
remote func player_emote(player_id, emote_id):
    get_tree().get_root().get_node("gamestate").player_emote(player_id, emote_id)