extends Node

const PORT = 10567
const MAX_PLAYERS = 200

var players = {}


func _ready():
    get_tree().connect("network_peer_connected", self, "_client_connected")
    get_tree().connect("network_peer_disconnected", self, "_client_disconnected")
    
    var server = NetworkedMultiplayerENet.new()
    server.create_server(PORT, MAX_PLAYERS)
    get_tree().set_network_peer(server)
    print("started successfully")

func _client_connected(id):
    print("a client connected: " + str(id))
    var new_client = load("res://remote_client.tscn").instance()
    new_client.set_name(str(id))
    get_tree().get_root().add_child(new_client)
    
func _client_disconnected(id):
    print("a client disconnected: " + str(id))
    get_tree().get_root().get_node(str(id)).queue_free()
    unregister_player(id)
    
remote func register_player(player_id, player_name):
    print("registered " + str(player_id))
    for i in players:
        rpc_id(i, "register_player", player_id, player_name)
    players[player_id] = player_name
    for i in players.keys():
        rpc_id(player_id, "register_player", i, players[i])
    
remote func unregister_player(player_id):
    for i in players.keys():
        rpc_id(i, "unregister_player", player_id)
    players.erase(player_id)
    
remote func player_emote(player_id, emote_id):
    for i in players.keys():
        rpc_id(i, "player_emote", player_id, emote_id)